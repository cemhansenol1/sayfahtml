from flask import Flask

app = Flask(__name__)
app.register_blueprint(mod_index)

if __name__ == '__main__':
    app.run()
