from app import app
from flask import render_template

@app.route("/hakkimizda")
def hakkimizda():
    return render_template("hakkimizda.html")
