from app import app
from flask import render_template


@app.route("/iletisim")
def iletisim():
    return render_template('iletisim.html')
